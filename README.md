# HTML/CSS markup (full-responsive markup, pixel perfect) #

Available on [http://net-s.pusku.com/semi/](http://net-s.pusku.com/semi/)

* Minimally compatible with:
    * Internet Explorer 10
    * Chrome 30
    * Safari 6.1
    * Firefox 30

## Google pagespeed insights result: ##

Mobile: 91/100

https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fnet-s.pusku.com%2Fsemi%2F&tab=mobile

Desktop: 97/100

https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fnet-s.pusku.com%2Fsemi%2F&tab=desktop


## Mokups and pixel perfect overlay (only desktop mockup were provided) ##

![semi-mockup.jpg](https://bitbucket.org/repo/kMkrRob/images/1746126418-semi-mockup.jpg)
![semi-overlayed-pixel-perfect.jpg](https://bitbucket.org/repo/kMkrRob/images/2301819465-semi-overlayed-pixel-perfect.jpg)